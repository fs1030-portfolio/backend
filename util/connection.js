import { createConnection } from "mysql";
import dotenv from "dotenv";
dotenv.config();

var connection = createConnection({
  socketPath: process.env.DATABASE_SOCKET,
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME
});

connection.connect(function (err) {
  if (err) {
    console.error("error connecting: " + err.stack);
    return;
  }

  console.log("connected as id " + connection.threadId);
});

export default connection;
