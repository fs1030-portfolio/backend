// Function using regex to check for valid e-mail
const validateEmail = (email) => {
    const emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return emailReg.test(email.toLowerCase())
}

// Function to check if all fields are in object
const contactFields = (obj) => {
    let err = []
    let requiredFields = ['name', 'email', 'phoneNumber', 'content']

    requiredFields.forEach(requiredField => {
        if (obj[requiredField] === undefined) {
            err.push(requiredField)
        }
    })

    return err
}

// Function to check if all values are in object (including valid e-mail)
const contactValues = (obj) => {
    let err = []

    for (const prop in obj) {
        if (prop === 'email') {
            !validateEmail(obj[prop]) ? err.push(prop) : ''
        } else if (obj[prop] == '') {
            err.push(prop)
        }
    }
    return err
}

// Middleware to check if there are no errors
const contactObj = (req, res, next) => {
    let contact = req.body
    let field = contactFields(contact)
    let value = contactValues(contact)
    if (field.length === 0 && value.length === 0) {
        next()
    } else {
        let err = []
        let invalid = {
            message: "validation error",
            invalid: err.concat(field, value)
        }
        return res.status(400).json(invalid)
    }
}

// Function to check if all fields are in object
const userFields = (obj) => {
    let err = []
    let requiredFields = ['name', 'password', 'email']

    requiredFields.forEach(requiredField => {
        if (obj[requiredField] === undefined) {
            err.push(requiredField)
        }
    })

    return err
}

// Function to check if all values are in object (including valid e-mail and password)
const userValues = (obj) => {
    let err = []

    for (const prop in obj) {
        if (prop === 'email') {
            !validateEmail(obj[prop]) ? err.push(prop) : ''
        } else if (prop === 'password') {
            obj[prop].length >= 8 ? '' : err.push(prop)
        } else if (obj[prop] == '') {
            err.push(prop)
        }
    }
    return err
}

// Middleware to check if there are no errors
const userObj = (req, res, next) => {
    let user = req.body
    let field = userFields(user)
    let value = userValues(user)
    
    if (field.length === 0 && value.length === 0) {
        next()
    } else {
        let err = []
        let invalid = {
            message: "validation error",
            invalid: err.concat(field, value)
        }
        return res.status(400).json(invalid)
    }    
}

export default { contactObj, userObj }