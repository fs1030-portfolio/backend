import express from 'express'
import jwtVerify from '../middleware/jwtVerify.js'
import connection from '../util/connection.js'
const router = express.Router()

// Route to post a new resume page entry
router.post('/admin/resume/add',jwtVerify, (req, res) => {
    let newEntry = req.body

    connection.query('INSERT INTO resume SET ?', newEntry, function(err, result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else {
            return res.status(200).json(result)
        }
    })
})

// Route to delete a resume page entry
router.delete('/admin/resume/delete/:id',jwtVerify, (req, res) => {
    let id = req.params.id

    connection.query('DELETE FROM resume WHERE id=?', id, function(err, result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else {
            return res.status(200).json(result)
        }
    })
})

// Route to display all resume entries to administrator (Required Verification)
router.get('/admin/resume', jwtVerify, (req, res) => {
    connection.query('SELECT * FROM resume', function(err, result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else {
            return res.status(200).json(result)
        }
    })
})

// Route to update specific resume to administrator (Required Verification)
router.patch('/admin/resume/edit/:id', jwtVerify, (req, res) => {
    const {title, content} = req.body

    connection.query(`UPDATE resume SET title="${title}", content="${content}" WHERE id="${req.params.id}"`, function(err,result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else {
            return res.status(200).json(result)
        }
    })
})


// Route to display resume to site
router.get('/resume', (req, res) => {
    connection.query('SELECT * FROM resume', function(err, result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else {
            return res.status(200).json(result)
        }
    })
})

export default router