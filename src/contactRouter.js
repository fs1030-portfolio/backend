import express from 'express'
import { v4 as uuidv4 } from 'uuid'
import valid from '../middleware/validation.js'
import jwtVerify from '../middleware/jwtVerify.js'
import connection from '../util/connection.js'
const router = express.Router()

// Route to display all entries (Required Verification)
router.get('/contact_form/entries', jwtVerify, (req, res) => {
    connection.query('SELECT * FROM contactentries', function(err, result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else {
            return res.status(200).json(result)
        }
    })
})

// Route to display one entry by ID (Required Verification)
router.get('/contact_form/entries/:id', jwtVerify, (req, res) => {
    const id = req.params.id

    connection.query('SELECT * FROM contactentries WHERE id = ?', id, function(err,result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else if (result.length > 0) {
            return res.status(200).json(result)
        } else {
            return res.status(404).json({message: `${id} not found`})
        }
    })
})

// Route to post an entry
router.post('/contact_form/entries', valid.contactObj, (req, res) => {
    let newEntry = {id: uuidv4(), ...req.body}

    connection.query('INSERT INTO contactentries SET ?', newEntry, function(err, result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else {
            return res.status(200).json(newEntry)
        }
    })
})

// Route to delete a contact entry
router.delete('/contact_form/entries/delete/:id',jwtVerify, (req, res) => {
    let id = req.params.id

    connection.query('DELETE FROM contactentries WHERE id=?', id, function(err, result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else {
            return res.status(200).json(result)
        }
    })
})

export default router