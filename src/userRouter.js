import express from 'express'
import { v4 as uuidv4 } from 'uuid'
import valid from '../middleware/validation.js'
import bcrypt from 'bcrypt'
import connection from '../util/connection.js'
import jwtVerify from '../middleware/jwtVerify.js'
const router = express.Router()

// Route to post a user
router.post('/users', valid.userObj, jwtVerify, (req, res) => {
    let newUser = {id: uuidv4(), ...req.body}

    const hash = bcrypt.hashSync(newUser.password, 10)
    newUser.password = hash

    connection.query('INSERT INTO user SET ?', newUser, function (err, result) {
        if (err) {
            console.log(err)
            if (err.code = 'ER_DUP_ENTRY') { return res.status(400).json({message: 'e-mail address already exists'})}
            return res.status(500).json({message: 'unable to insert data'})
        } else {
            let {password, ...displayUser} = newUser
            return res.status(200).json(displayUser)
        }
    })
})

export default router