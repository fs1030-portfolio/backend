import express from 'express'
import jwtVerify from '../middleware/jwtVerify.js'
import connection from '../util/connection.js'
const router = express.Router()

// Route to post a new portfolio page entry
router.post('/admin/portfolio/add',jwtVerify, (req, res) => {
    let newEntry = req.body

    connection.query('INSERT INTO portfolio SET ?', newEntry, function(err, result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else {
            return res.status(200).json(result)
        }
    })
})

// Route to delete a portfolio page entry
router.delete('/admin/portfolio/delete/:id',jwtVerify, (req, res) => {
    let id = req.params.id

    connection.query('DELETE FROM portfolio WHERE id=?', id, function(err, result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else {
            return res.status(200).json(result)
        }
    })
})

// Route to update specific portfolio to administrator (Required Verification)
router.patch('/admin/portfolio/edit/:id', jwtVerify, (req, res) => {
    const {title, content, url} = req.body

    connection.query(`UPDATE portfolio SET title="${title}", content="${content}", url="${url}" WHERE id="${req.params.id}"`, function(err,result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else {
            return res.status(200).json(result)
        }
    })
})


// Route to display all portfolio entries to administrator (Required Verification)
router.get('/admin/portfolio', jwtVerify, (req, res) => {
    connection.query('SELECT * FROM portfolio', function(err, result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else {
            return res.status(200).json(result)
        }
    })
})

// Route to display portfolio to site
router.get('/portfolio', (req, res) => {
    connection.query('SELECT * FROM portfolio', function(err, result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else {
            return res.status(200).json(result)
        }
    })
})

export default router