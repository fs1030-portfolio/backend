import express from 'express'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'
import connection from '../util/connection.js'
const router = express.Router()

// Route to generate token for authorization
router.post('/auth', (req, res) => {
    const username = req.body.email
    const password = req.body.password

    if (username && password) {
        connection.query('SELECT password FROM user WHERE email = ?', username, function (err,result) {
            if (err) {
                console.log(err)
                return res.status(500).json({message: 'unable to retrieve data'})
            } else if (result.length > 0) {
                let dbPass = result[0].password
                const match = bcrypt.compareSync(password, dbPass)
                if (match) {
                    const token = jwt.sign({username}, process.env.JWT_SECRET, {expiresIn: '1h'})
                    return res.json({token})
                }
            }
        })
    } else {
        return res.status(401).json({message: 'incorrect credentials provided'})
    }
})

export default router