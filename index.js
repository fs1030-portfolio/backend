import express from "express";
import userRouter from "./src/userRouter.js";
import contactRouter from "./src/contactRouter.js";
import resumeRouter from "./src/resumeRouter.js";
import portfolioRouter from "./src/portfolioRouter.js";
import authRouter from "./src/authRouter.js";
import errorHandler from "./middleware/errorHandler.js";
import cors from "cors";
import dotenv from "dotenv";
dotenv.config();

const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(cors());

app.get("/", (req, res) => res.json("Welcome"));

app.use(userRouter);
app.use(authRouter);
app.use(contactRouter);
app.use(resumeRouter);
app.use(portfolioRouter);
app.use(errorHandler);

// Catch all error for post and get if wrong URL's are used
app.get("*", (req, res, next) => {
  let err = Error("Wrong URL entered")
  next(err)
})

app.post("*", (req, res, next) => {
  let err = Error("Wrong URL entered")
  next(err)
})

app.listen(PORT, () =>
  console.log(`API server ready on http://localhost:${PORT}`)
);
