FROM node:lts-slim

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY ./ ./

COPY wait-for-it.sh ./

RUN chmod +x wait-for-it.sh

ARG JWT_SECRET

ENV JWT_SECRET=$JWT_SECRET

CMD npm run start
