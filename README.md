# Chris Germishuy Backend

# Using Docker to run this project

1. You can run this project using docker compose
2. Clone the repo using SSH or HTTP
3. Ensure that Docker is installed
4. Update the JWT_SECRET variable in the Dockerfile if you wish to run this locally using docker compose
5. Use the following command to start this application:

docker compose -f docker-compose.yml up

6. This should start both the API and the MySQL server required for this project
7. Connect to the SQL server and run the portfolio.sql file in your preferred SQL administration software
8. For the frontend portion of this, you can clone the following repository and follow the instructions there:

https://gitlab.com/fs1030-portfolio/frontend

# Run this project using npm and nodemon

1. To run this project you will need to clone this repo using SSH or HTTP
2. Open the folder in VS Code or applicable source-code editor with access to terminal
3. When in the terminal, use npm install to install node dependencies
4. The project uses http://localhost:4000 to run so ensure that port 4000 is not in use (you can change the port by going into .env and adjusting the variable of port from 4000 to something else)
5. nodemon is installed for this application. You can use 'npm run dev' in the terminal to run this application in dev mode. Whenever changes are detected in the code, it'll automatically restart restart the application and pick up the new code.

# Information on the portfolio.sql file and database

1. To ensure that the frontend works correctly, please run the portfolio.sql file in your preferred SQL administration software (MySQL Workbench, WinSQL etc.)
2. Create a user in your database that matches the information in the connection.js file. You can do this using the following SQL statement:

CREATE USER 'nodeuser'@'localhost' IDENTIFIED BY 'portfolio1234#';
GRANT ALL PRIVILEGES ON * . * TO 'nodeuser'@'localhost';
FLUSH PRIVILEGES;

3. To test if your connection is successful, type the following in your terminal:

node .\util\connection.js

This should say connected with an id. 

# Information about the .env file

1. This project uses the .env file to store important information for this application
2. Rename the .envexample to .env and adjust the properties in this file accordingly
3. PORT refers to the port this application will use on your system
4. JWT_Secret will sign and issue tokens for this application and can be used later for verification

# Ready to Go

After the steps above have been completed, you should be ready to go and you can run the backend using the following in your terminal:

npm start
OR
npm run dev
